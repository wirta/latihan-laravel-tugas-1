 <?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route Latihan: 
Route::get('/', 'DashboardController@index');
Route::get('/biodata', 'FormController@bio');
Route::post('/kirim', 'FormController@kirim');
Route::get('/data-table', 'DashboardController@table');


Route::get('/', 'IndexController@index');
Route::get('/register', 'AuthController@bio');
Route::post('/welcome', 'AuthController@kirim');
Route::get('/data-table', 'IndexController@table');
*/

// CRUD CAST
//Create
Route::get('/cast/create', 'CastController@create'); //mengarah ke form tambah data
Route::post('/cast', 'CastController@store'); //menyimpan data form ke database table cast

//Read
Route::get('/cast', 'CastController@index'); //ambil data ke data ditampilkan di blade
Route::get('/cast/{cast_id}', 'CastController@show'); //Route detail Caster

//Update
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //Route untuk mengarah ke form edit
Route::put('/cast/{cast_id}', 'CastController@update'); //Route untuk mengarah ke form edit

//Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy'); //Route delete data berdasarkan id 

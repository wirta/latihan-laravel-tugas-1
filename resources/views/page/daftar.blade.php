@extends('layout.master')
@section('title')
Halaman Utama Dashboard
@endsection 
@section('content')
    <form action="/welcome" method="post">
        @csrf

        <h1>Buat Account Baru</h1><br>  
        <h4>Sign Up Form</h4>  
          <label> First Name</label><br>
          <input type="text" name="nama_depan"><br><br>
          <label> Last Name</label><br>
          <input type="text" name="nama_belakang"><br><br>
          <label>Gender</label><br>
          <input type="radio" value="1" name="jk"> Laki-laki<br>
          <input type="radio" value="2" name="jk">Perempuan<br><br>
      
          <label>Nationality</label><br>
          <select name="bahasa" id="">
            <option value="1">Indonesia</option>
            <option value="2">Amerika</option>
            <option value="3">Inggris</option>
          </select><br><br>
      
          <label>Language Spoken</label><br>
            <input type="checkbox" value="1" name="language"> Bahasa Indonesia <br>
            <input type="checkbox" value="2" name="language"> English <br>
            <input type="checkbox" value="3" name="language"> Other <br><br>
      
          <label>Bio</label><br>
            <textarea name="bio" cols="30" rows="10"></textarea><br><br> 
                    
        </form>
      
        <input type="Submit" value="Sign Up"><br> 

    <a href="/">Kembali ke halaman Home</a>
@endsection
@extends('layout.master')
@section('title')
Halaman Edit Bintang Film
@endsection 
@section('content')

<form method="POST" action="/cast/{{$cast->id}}">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama Caster</label>
      <input type="text" name="nama" value="{{$cast->nama}}" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message}}</div>  
    @enderror 
    <div class="form-group">
        <label>Umur</label>
        <input type="number" name="umur" value="{{$cast->umur}}" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message}}</div>  
    @enderror
    <div class="form-group">
        <label>Biografi</label>
        <textarea name="bio" cols="30" rows="5" class="form-control"> {{$cast->nama}} </textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message}}</div>  
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection 
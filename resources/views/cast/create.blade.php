@extends('layout.master')
@section('title')
Halaman Tambah Caster
@endsection 
@section('content')

<form method="POST" action="/cast">
    @csrf
    <div class="form-group">
      <label>Nama Caster</label>
      <input type="text" name="nama" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message}}</div>  
    @enderror 
    <div class="form-group">
        <label>Umur</label>
        <input type="number" name="umur" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message}}</div>  
    @enderror
    <div class="form-group">
        <label>Biografi</label>
        <textarea name="bio" cols="20" rows="5" class="form-control"></textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message}}</div>  
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection 
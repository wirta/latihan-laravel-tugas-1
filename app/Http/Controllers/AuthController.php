<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio()
    {
        return view('page.daftar');
    }

    public function kirim(Request $request)
    {
      //  dd($request->all());
       
        $namadepan = $request['nama_depan'];
        $namabelakang = $request['nama_belakang'];
        $gender = $request['jk'];
        $nationality = $request['bahasa'];
        $spoken = $request['language'];
        $datadiri = $request['bio'];


        return view('page.welcome', compact("namadepan", "namabelakang", "gender", "nationality", "spoken", "datadiri"));
  
    }
}

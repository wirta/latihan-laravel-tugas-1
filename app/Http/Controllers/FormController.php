<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function bio()
    {
        return view('page.daftar');
    }
    public function kirim(Request $request)
    {
        //dd($request->all());
        $nama = $request['nama_lengkap'];
        $alamat = $request['adress'];

        return view('page.welcome', compact("nama", "alamat"));
    }
}
